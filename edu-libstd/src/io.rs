//! IO functions for the application. (`println!`)

use crate::{arch::syscall, syscall::Syscall};
use alloc::string::String;
use core::fmt::{self, Write};

/// Prints to the console.
///
/// Taken from `std`.
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::io::_print(core::format_args!($($arg)*)));
}

/// Prints to the console, with a newline.
///
/// Taken from `std`.
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ({
        $crate::io::_print(core::format_args_nl!($($arg)*));
    })
}

/// Prints to the standard output.
///
/// Do not use directly, prefer [`print`] and [`println`].
pub fn _print(args: fmt::Arguments<'_>) {
	// Format and print formatted `str`.
	let mut s = String::new();
	write!(&mut s, "{}", args).unwrap();
	unsafe { syscall::syscall2(Syscall::Print as _, s.as_ptr() as _, s.len()) };
}

#[cfg(test)]
mod tests {
	#[test]
	fn printtest() {
		print!("testprint");
		println!(" - testprintln");
	}
}
