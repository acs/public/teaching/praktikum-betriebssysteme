#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[no_mangle]
static FORCE_LINK: u8 = 0;

use alloc::vec::Vec;
use edu_kernel::sync::spinlock::Spinlock;
use edu_kernel::{arch::delay, println, scheduler::thread};

const NR_STEPS: usize = 100;
const WIDTH: f64 = 1.0 / (NR_STEPS as f64);

static SUM: Spinlock<f64> = Spinlock::new(0.0);

fn pi_parallel(range: core::ops::Range<usize>) {
	range.for_each(|i| {
		let x = (i as f64 + 0.5) * WIDTH;
		SUM.lock(|s| {
			let s_tmp = *s;
			// Because we run single-core, we need to insert a delay here to provoke
			// generate a race-condition
			delay(1000000);
			*s = s_tmp + 4.0 / (1.0 + x * x);
		});
	});
}


/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	let mut threads = Vec::new();

	threads.push(thread::spawn(|| pi_parallel(0..NR_STEPS / 2)));
	threads.push(thread::spawn(|| pi_parallel(NR_STEPS / 2..NR_STEPS)));

	// wait for all threads to finish
	for t in threads {
		t.join();
	}

	//let pi = WIDTH * unsafe { SUM };
	let pi = WIDTH * SUM.lock(|s| *s);
	println!("pi {}", pi);

	assert!(3.1415 - pi < 0.01);
}
