//! Task management

pub mod context;
pub mod fifo_scheduler;
pub mod priority_scheduler;
pub mod task;
pub mod thread;

use self::{priority_scheduler::PriorityScheduler, task::Priority};
use crate::scheduler::task::{Task, TaskStatus};
use crate::{
	arch::{self, interrupts, processor},
	sync::spinlock::Spinlock,
};
use crate::{run_once, sync::interrupt::SingleCoreInterruptMutex};
use alloc::{boxed::Box, sync::Arc};
use core::mem;
use log::{debug, error, info, warn};

pub type TaskManagerMutex = SingleCoreInterruptMutex<TaskManager>;
static mut TASK_MANAGER: Option<TaskManagerMutex> = None;

/// Initialite module, must be called once, and only once
pub fn init() {
	info!("Initializing scheduler");

	custom_init(Box::new(PriorityScheduler::new()));
}

/// Initialite module, but with a custom scheduler. Must be called once, and
/// only once, and [init] must not be called then.
pub fn custom_init<S: 'static + Scheduler>(scheduler: Box<S>) {
	// Ensure that a second initialization fails
	run_once!().unwrap();
	unsafe {
		TASK_MANAGER = Some(SingleCoreInterruptMutex::new(TaskManager::new(scheduler)));
	}

	arch::register_task();
}

/// Returns a reference to this core's task manager.
pub fn get() -> &'static TaskManagerMutex {
	// SAFETY: The only critical section — schedule — is wrapped in yield_now.
	unsafe { TASK_MANAGER.as_ref().expect("Scheduling not initalized") }
}

/// Resumes preemption.
///
/// # Safety
///
/// Must be called after each potential context switch.
pub unsafe fn resume_preemption() {
	get().force_unlock();
	interrupts::enable();
	arch::resume_preemption();
}

/// A reference to a [`Task`].
///
/// This reference can be cloned and is [`Send`] and [`Sync`].
pub type TaskRef = Arc<Spinlock<Task>>;

/// A scheduler.
///
/// Schedulers are used to manage all tasks which are ready to run.
///
/// This trait can be used to implement different scheduling algorithms.
/// Implementors can be directly plugged into [`TaskManager`].
pub trait Scheduler {
	/// Adds a new task to the scheduler.
	fn insert(&mut self, task: TaskRef);

	/// Returns the next task to execute.
	///
	/// Returns [`None`], if no tasks are available.
	fn next(&mut self) -> Option<TaskRef>;

	/// Wakes `task` up and adds it to the scheduler.
	fn wakeup(&mut self, task: TaskRef) {
		task.lock(|task| {
			debug!("Waking up task {}", task.id());
			assert_eq!(
				task.status,
				TaskStatus::Blocked,
				"Can only wake up blocked tasks."
			);

			task.status = TaskStatus::Ready;
		});
		self.insert(task)
	}
}

/// A single-core task manager.
///
/// This task manager is generic over a [`Scheduler`].
///
/// Not to be confused with one of the most popular Microsoft applications.
pub struct TaskManager {
	/// The current task.
	current_task: TaskRef,
	/// A finished task, ready for being freed.
	finished_task: Option<TaskRef>,
	/// The task scheduler.
	scheduler: Box<dyn Scheduler>,
}

impl TaskManager {
	/// Constructs a new task manager from the supplied scheduler.
	pub fn new<S: 'static + Scheduler>(scheduler: Box<S>) -> Self {
		let boot_task = Arc::new(Spinlock::new(Task::new_boot()));

		TaskManager {
			current_task: boot_task,
			finished_task: None,
			scheduler,
		}
	}

	/// Spawns a new kernelspace task.
	pub fn spawn_kernel(&mut self, f: extern "C" fn(*mut ()), arg: *mut ()) -> TaskRef {
		self.spawn_task(Task::new_kernel(f, arg))
	}

	/// Spawns a new kernelspace task with the specified priority.
	pub fn spawn_kernel_prio(
		&mut self,
		f: extern "C" fn(*mut ()),
		arg: *mut (),
		prio: Priority,
	) -> TaskRef {
		self.spawn_task(Task::kernel_from_priority(f, arg, prio))
	}

	/// Spawns a new userspace task.
	pub fn spawn_user(&mut self, f: extern "C" fn(*mut ()) -> !, arg: *mut ()) -> TaskRef {
		self.spawn_task(Task::new_user(f, arg))
	}

	/// Spawns a new userspace task with the specified priority.
	pub fn spawn_user_prio(
		&mut self,
		f: extern "C" fn(*mut ()) -> !,
		arg: *mut (),
		prio: Priority,
	) -> TaskRef {
		self.spawn_task(Task::user_from_priority(f, arg, prio))
	}

	/// Spawns a task.
	fn spawn_task(&mut self, task: Task) -> TaskRef {
		let task = Arc::new(Spinlock::new(task));
		self.scheduler.insert(task.clone());
		task
	}

	/// Exits the current task.
	pub fn exit(mutex: &'_ TaskManagerMutex, result: Result<(), ()>) -> ! {
		assert!(
			interrupts::are_enabled(),
			"Interrupts must be enabled when exiting, as exiting yields."
		);
		mutex.lock(|this| {
			let Self {
				current_task,
				scheduler,
				..
			} = this;
			current_task.lock(|task| {
				match result {
					Ok(()) => info!("Finishing task {}", task.id()),
					Err(()) => error!("Aborting task {}", task.id()),
				}

				// TODO: destroy user space
				// drop_user_space();

				task.status = TaskStatus::Finished;

				// Wakup joining tasks
				for task in task.joining.drain(..) {
					scheduler.wakeup(task)
				}
			});
			// SAFETY: This is the end of this top-level interrupt-free section
			unsafe { this.schedule() }
		});
		unreachable!()
	}

	/// Blocks the current task.
	///
	/// This sets the current task to [`TaskStatus::Blocked`] and returns the
	/// [`TaskRef`]. This function does not yield the current task. Instead you
	/// should store the returned task and prepare it for eventual wakeup. After
	/// storing the task away, call [`yield_now`](Self::yield_now) manually!
	pub fn block_current_task(&mut self) -> TaskRef {
		self.current_task.lock(|task| {
			debug!("Blocking task {}", task.id());
			assert_eq!(
				task.status,
				TaskStatus::Running,
				"Can only block running tasks."
			);

			task.status = TaskStatus::Blocked;
		});
		self.current_task.clone()
	}

	/// Wakes `task` up and adds it to the scheduler.
	pub fn wakeup_task(&mut self, task: TaskRef) {
		self.scheduler.wakeup(task)
	}

	/// Waits for `task` to finish.
	pub fn join(mutex: &'_ TaskManagerMutex, task: TaskRef) {
		assert!(
			interrupts::are_enabled(),
			"Interrupts must be enabled when joining, as joining might yield."
		);
		mutex.lock(|this| {
			let finished = task.lock(|task| {
				warn!("Joining {}", task.id());
				let finished = task.status == TaskStatus::Finished;
				if !finished {
					let blocked = this.block_current_task();
					task.joining.push(blocked);
				}
				finished
			});
			if !finished {
				// SAFETY: This is the end of this top-level interrupt-free section
				unsafe { this.schedule() }
			}
		});
	}

	/// Returns the top of the current task's stack
	pub fn get_current_stack_top(&self) -> usize {
		self.current_task.lock(|task| task.context().stack().top())
	}

	/// Yields the current task.
	pub fn yield_now(mutex: &'_ TaskManagerMutex) {
		assert!(
			interrupts::are_enabled(),
			"Interrupts must be enabled when yielding. \
			Interrupt-free sections are not exclusive on a single core, if they yield."
		);
		mutex.lock(|this| unsafe { this.schedule() })
	}

	/// Schedules and may switch to another task.
	///
	/// Must be called at the end of an interrupt-free section, since calling
	/// this potentially yields to another task. Prefer
	/// [`yield_now`](Self::yield_now) over manually handling this requirement.
	pub unsafe fn schedule(&mut self) {
		// Clean up any finished task.
		drop(self.finished_task.take());

		let current_status = self.current_task.lock(|task| task.status);
		match current_status {
			TaskStatus::Running => {
				self.scheduler.insert(self.current_task.clone());
				let new_task = self.scheduler.next().unwrap();
				if !Arc::ptr_eq(&self.current_task, &new_task) {
					// Switch to another task.
					self.current_task
						.lock(|task| task.status = TaskStatus::Ready);
					self.switch(new_task)
				} else {
					// Continue with the current task.
				}
			}
			TaskStatus::Finished => {
				if let Some(new_task) = self.scheduler.next() {
					self.finished_task
						.replace(self.current_task.clone())
						.unwrap_none();
					self.switch(new_task)
				} else {
					// All tasks finished
					processor::exit(0)
				}
			}
			TaskStatus::Blocked => {
				// If the current task is blocked, there must be a ready task.
				let new_task = self.scheduler.next().unwrap();
				self.switch(new_task)
			}
			TaskStatus::Ready => unreachable!("Invalid state"),
		}

		// It is enforced that this is at the end of a interrupt-free section.
		resume_preemption()
	}

	/// Switches to `task`.
	fn switch(&mut self, task: TaskRef) {
		debug!(
			"Switching from task {} to task {}.",
			self.current_task.lock(|task| task.id()),
			task.lock(|task| task.id())
		);

		let old_task = mem::replace(&mut self.current_task, task);
		self.current_task
			.lock(|task| task.status = TaskStatus::Running);

		// Drop this task before switching out of it — we might never return.
		// self.finished task ensures, this is still valid and will clean up
		// eventually.
		//
		// The mutex is cheated here. Otherwise, it would still be locked after the
		// context switch. This would lead to issues when scheduling again.
		let old_ref = unsafe { &mut *(Arc::as_ptr(&old_task) as *mut Spinlock<Task>) }.get_mut();
		drop(old_task);
		let new_ref =
			unsafe { &mut *(Arc::as_ptr(&self.current_task) as *mut Spinlock<Task>) }.get_mut();

		old_ref.switch(new_ref);
	}
}

impl Default for TaskManager {
	fn default() -> Self {
		let sched = Box::new(PriorityScheduler::new());
		Self::new(sched)
	}
}
